//
//  NSString+EventTableCellDrawing.m
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project07.
//  Copyright (c) 2014年 Masatoshi Nishikata. All rights reserved.
//

#import "NSString+EventTableCellDrawing.h"

@implementation NSString (EventTableCellDrawing)

-(NSString*)truncatedStringWithFont:(UIFont*)font width:(CGFloat)width
{
	NSString* hogeString = [self copy];
	
	while(1)
	{
		if( [hogeString length] <= 2 ) break;
		
		@autoreleasepool {
			
			
			CGFloat indefiniteWidth = [hogeString sizeWithFont:font forWidth:MAXFLOAT lineBreakMode:UILineBreakModeTailTruncation].width;
			CGFloat constrainedWidth = [hogeString sizeWithFont:font forWidth:width lineBreakMode:UILineBreakModeTailTruncation].width;
			
			if( indefiniteWidth == constrainedWidth ) return hogeString;
			
			hogeString = [self substringToIndex: [hogeString length]-2];
			hogeString = [hogeString stringByAppendingString:NSLocalizedString(@"Truncate Symbol",@"")];
			
		}
	}
	
	return hogeString;
}

-(NSString*)truncatedStringWithCTFont:(CTFontRef)font width:(CGFloat)width
{
	NSString* hogeString = [self copy];
	CGSize boundingRect = CGSizeMake(MAXFLOAT, MAXFLOAT);
	
	NSDictionary* attributes = @{ (__bridge id)kCTFontAttributeName: (__bridge id)font };
	CGSize size = [hogeString sizeWithAttributes:attributes];

	if( size.width < width ) return hogeString;
	
	while(1)
	{
		if( [hogeString length] <= 2 ) break;
		
		@autoreleasepool {
			
			CGSize size = [hogeString sizeWithAttributes:attributes];
			
			
			if( size.width < width ) return hogeString;
			
			hogeString = [self substringToIndex: [hogeString length]-2];
			hogeString = [hogeString stringByAppendingString:NSLocalizedString(@"Truncate Symbol",@"")];
			
		}
	}
	
	return hogeString;
}

@end
