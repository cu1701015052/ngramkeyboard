//
//  Mozc.h
//  Mozc
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project20.
//  Copyright (c) 2014年 Masatoshi Nishikata. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Mozc.
FOUNDATION_EXPORT double MozcVersionNumber;

//! Project version string for Mozc.
FOUNDATION_EXPORT const unsigned char MozcVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Mozc/PublicHeader.h>


#import <Mozc/InputManager.h>
