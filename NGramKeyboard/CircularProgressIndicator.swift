//
//  CircularProgressIndicator.swift
//  LawInSwift
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2015 Masatoshi Nishikata. All rights reserved.
//

import Foundation
import UIKit

final class CircularProgressIndicator: UIView {
  
  static var count = 0
  var indeterminate: Bool = true {
    didSet {
      if oldValue == true && indeterminate == true && mDisplayLink != nil { return }
      if oldValue == false && indeterminate == false { return }

      mDisplayLink?.invalidate()
      mDisplayLink = nil
      
      animationStartProgress = nil
      animationCurrentProgress = 0
      animationInterval = 0
      
      if indeterminate == true {
        mDisplayLink = CADisplayLink(target: self, selector: #selector(self.displayAnimation))
        mDisplayLink!.frameInterval = 1
        mDisplayLink!.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
      }
    }
  }
  private var mDisplayLink: CADisplayLink? = nil
  private var previousProgress: Double = 0.0
  private var animationStartProgress: Double? = 0.0
  private var animationEndProgress: Double = 0.0
  private var animationCurrentProgress: Double = 0.0
  private let animationSpeed = 0.1
  private var animationInterval: Double = 0
 var lineWidth: CGFloat = 1.25
  var progress: Double = 0.0 {
    willSet {
      previousProgress = progress
      
      if indeterminate == true && previousProgress > 0  {
        indeterminate = false
      }
    }
    
    didSet {
      animationStartProgress =  animationCurrentProgress
      animationEndProgress = progress
      
      if progress > 0 && mDisplayLink == nil { // Animation start
        animationInterval = 0
        
        DispatchQueue.main.async {
          self.mDisplayLink?.invalidate()
          self.mDisplayLink = CADisplayLink(target: self, selector: #selector(self.displayAnimation))
          self.mDisplayLink!.frameInterval = 1
          self.mDisplayLink!.add(to: .current, forMode: .common)
        }
      }
    }
  }
  private var observedToken: NSKeyValueObservation? = nil
  var observedProgress: Progress? {
    didSet {
      observedToken?.invalidate()
      observedToken = observedProgress?.observe(\.fractionCompleted) { [weak self] object, change in
        DispatchQueue.main.async {
          if let progress = self?.observedProgress {
            self?.progress = progress.fractionCompleted
          }
        }
      }
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    CircularProgressIndicator.count += 1
    backgroundColor = UIColor.clear
    layer.drawsAsynchronously = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder:aDecoder)
    
    backgroundColor = UIColor.clear
    layer.drawsAsynchronously = true
  }
  
  deinit {
    CircularProgressIndicator.count -= 1
  }
  
  override func removeFromSuperview() {
    super.removeFromSuperview()
    mDisplayLink?.invalidate()
    mDisplayLink = nil
  }
  
  private func draw(from startAngle: CGFloat, to endAngle: CGFloat ) {
    let ctx = UIGraphicsGetCurrentContext()
    
    let point = CGPoint(x: self.bounds.midX,y: self.bounds.midY)
    let radius: CGFloat = self.bounds.size.width / 2 - 3
    let path = UIBezierPath(arcCenter: point, radius: radius, startAngle: startAngle - CGFloat(Double.pi / 2), endAngle: endAngle - CGFloat(Double.pi / 2), clockwise: true)
    
    path.lineWidth = lineWidth
    ctx?.setStrokeColor(tintColor.cgColor)
    path.stroke()
  }
  
  @objc func displayAnimation() {
    if superview == nil { return }
    
    if indeterminate == true {
      setNeedsDisplay()
      return
    }
    
    if animationStartProgress == nil { return }
    
    let thisInterval = Date().timeIntervalSinceReferenceDate
    if animationInterval == 0 { animationInterval = thisInterval }
    
    let elapsed = thisInterval - animationInterval
    
    // Max 0.5 sec
    
    if animationCurrentProgress >= animationEndProgress {
      // Finish
      
      mDisplayLink?.invalidate()
      mDisplayLink = nil
      return
    }
    
    let speed = ( animationEndProgress - animationStartProgress! ) / 0.5
    animationCurrentProgress = min( animationEndProgress,  animationStartProgress! + speed * elapsed )
    
    setNeedsDisplay()
  }
  
  override func draw(_ rect: CGRect) {
    
    if indeterminate == true {

      let thisInterval = Date().timeIntervalSinceReferenceDate
      if animationInterval == 0 { animationInterval = thisInterval }
      
      let elapsed = thisInterval - animationInterval
      let fraction: Double = 1.0/36.0 * 2.0 * .pi
      let animationCount = elapsed.truncatingRemainder(dividingBy: 1)  * 36
      let startAngle = CGFloat(Double(animationCount) * fraction)
      let endAngle = CGFloat(fraction * 30) + startAngle
      
      draw(from: startAngle, to: endAngle)
      
      return
    }
    
    if animationStartProgress == nil { return }
    
    let endAngle = 2 * .pi * animationCurrentProgress
    
    // Draw base circle
    let ctx = UIGraphicsGetCurrentContext();
    
    let point = CGPoint(x: self.bounds.midX,y: self.bounds.midY)
    let radius: CGFloat = self.bounds.size.width / 2 - 3
    let path = UIBezierPath(arcCenter: point, radius: radius, startAngle: 0, endAngle: CGFloat(2 * Double.pi), clockwise: true)
    
    path.lineWidth = lineWidth
    ctx?.setStrokeColor(tintColor.withAlphaComponent(0.2).cgColor)
    path.stroke()
    
    draw(from: 0, to: CGFloat(endAngle))
  }
}



