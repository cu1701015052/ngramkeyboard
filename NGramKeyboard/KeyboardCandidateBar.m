//
//  KeyboardCandidateBar.m
//  JapaneseKeyboardKit
//
//  Created by kishikawa katsumi on 2014/09/28.
//  Copyright (c) 2014 kishikawa katsumi. All rights reserved.
//

#import "KeyboardCandidateBar.h"
#import "KeyboardCandidateBarCell.h"
#import "InputCandidate.h"
#import "NSString+EventTableCellDrawing.h"
#import "UIImage+KeyboardButton.h"
#import "KeyboardFullscreenCandidateCollectionView.h"
#import "MozcKeyButton.h"

#define CANDIDATE_MAXIMUM 999

#define IS_RUNNING_ON_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define CANDIDATE_BAR_HEIGHT (IS_RUNNING_ON_IPAD?46:36)

@interface KeyboardCandidateBar () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic) MozcAlternativeButton* moreButton;
@property (nonatomic, strong) NSIndexPath* selectedIndexPath;
@end

@implementation KeyboardCandidateBar

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    [self commonInit:NO];
  }
  
  return self;
}

- (id)initWithFrame:(CGRect)frame fullscreen:(BOOL)fullscreen
{
  self = [super initWithFrame:frame];
  if (self) {
    
    self.fullscreen = fullscreen;
    
    if( fullscreen )
      [self commonInit:YES];
    else
      [self commonInit:NO];
  }
  
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self commonInit:NO];
  }
  
  return self;
}

/*
-(void)dealloc
{
  NSLog(@"*** KeyboardCandidateBar DEALLOC");
}*/

-(void)enable
{
  if( [self.collectionView isKindOfClass:[KeyboardFullscreenCandidateCollectionView class]] )
    [(KeyboardFullscreenCandidateCollectionView*)self.collectionView enable];
}

- (void)commonInit:(BOOL)fullscreen
{
  
  NSLayoutConstraint* constraint;
  
  self.backgroundColor = [UIColor lightGrayColor];
  @autoreleasepool {
    
    cellFont_ = (__bridge CTFontRef)([KeyboardCandidateBarCell font]);
  }
  
  
  if( !fullscreen )
  {
    
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    if( self.fullscreen )
    {
      self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
      
    }else
    {
      self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    
    
    self.flowLayout.minimumLineSpacing = 0.0;
    self.flowLayout.minimumInteritemSpacing = 0.0;
    //    self.flowLayout.sectionInset = UIEdgeInsetsMake(0.0, -1.0, 0.0, 0.0);
    
    UICollectionView* collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:self.flowLayout];
    self.collectionView = collectionView;
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.bounces = YES;
    self.collectionView.allowsMultipleSelection = NO;
    self.collectionView.showsVerticalScrollIndicator = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerClass:[KeyboardCandidateBarCell class] forCellWithReuseIdentifier:@"Cell"];
    [self addSubview:self.collectionView];
  }
  
  
  if( fullscreen )
  {
    KeyboardFullscreenCandidateCollectionView* myCollectionClassView = [[KeyboardFullscreenCandidateCollectionView alloc] initWithFrame:self.bounds];
    myCollectionClassView.delegate = self;
    
    self.collectionView = (id)myCollectionClassView;
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.collectionView];
  }
  
  
  /*
  constraint = [NSLayoutConstraint constraintWithItem: self.collectionView
                                            attribute: NSLayoutAttributeTop
                                            relatedBy: NSLayoutRelationEqual
                                               toItem: self
                                            attribute: NSLayoutAttributeTop
                                           multiplier: 1.0
                                             constant: self.fullscreen?CANDIDATE_BAR_HEIGHT:0];
  
  [self addConstraint:constraint];
  
  
  constraint = [NSLayoutConstraint constraintWithItem: self.collectionView
                                            attribute: NSLayoutAttributeBottom
                                            relatedBy: NSLayoutRelationEqual
                                               toItem: self
                                            attribute: NSLayoutAttributeBottom
                                           multiplier: 1.0
                                             constant: 0];
  
  [self addConstraint:constraint];
  
  constraint = [NSLayoutConstraint constraintWithItem: self.collectionView
                                            attribute: NSLayoutAttributeTrailing
                                            relatedBy: NSLayoutRelationEqual
                                               toItem: self
                                            attribute: NSLayoutAttributeTrailing
                                           multiplier: 1.0
                                             constant: 0];
  [self addConstraint:constraint];
  
  
  constraint = [NSLayoutConstraint constraintWithItem: self.collectionView
                                            attribute: NSLayoutAttributeLeading
                                            relatedBy: NSLayoutRelationEqual
                                               toItem: self
                                            attribute: NSLayoutAttributeLeading
                                           multiplier: 1.0
                                             constant: 0];
  [self addConstraint:constraint];
  */
  
  
  //	self.collectionView.contentInset = UIEdgeInsetsMake(0.0, -1.0 / scale, 0.0, 0.0);
  
  
//  self.moreButton = [[MozcAlternativeButton alloc] initWithFrame:CGRectMake(0, 0, 36, 36)];
//  self.moreButton.title = @"";
//  self.moreButton.tintColor = [UIColor blackColor];
//  NSBundle* bundle = [NSBundle bundleForClass:[self class]];
//  
//  self.moreButton.image = [UIImage imageNamed:@"ShowCandidates"
//                                     inBundle:bundle
//                compatibleWithTraitCollection:nil];
//  
//  
//  self.moreButton.hidden = YES;
//  [self.moreButton sizeToFit];
//  
//  self.moreButton.backgroundColor = [UIColor whiteColor];
//  CGRect buttonFrame = self.moreButton.frame;
//  
//  if( IS_RUNNING_ON_IPAD )
//  {
//    buttonFrame.size.width += 20;
//  }
//  
//  self.moreButton.frame = CGRectMake(self.bounds.size.width - buttonFrame.size.width, 0, buttonFrame.size.width, self.bounds.size.height);
//  self.moreButton.target = self;
//  self.moreButton.action = @selector(more:);
//  self.moreButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin ;
//  
//  [self addSubview:self.moreButton];
  
  
  CGFloat viewWidth = self.bounds.size.width;
  CGFloat viewHeight = self.bounds.size.height;
  
  CGFloat sepHeight = 1/[UIScreen mainScreen].scale;
  UIView *separator = [[UIView alloc] initWithFrame: CGRectMake(0, 0, viewWidth, sepHeight)];
  
  separator.contentScaleFactor = 0.2;
  separator.alpha = 0.5;
  separator.backgroundColor = [UIColor darkGrayColor];
  separator.userInteractionEnabled = NO;
  separator.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
  
  separator.contentMode = UIViewContentModeScaleToFill;
  
  [self addSubview: separator];
  
  //
  
  separator = [[UIView alloc] initWithFrame: CGRectMake(0, viewHeight-sepHeight, viewWidth, sepHeight)];
  separator.contentScaleFactor = 0.2;
  separator.backgroundColor = [UIColor darkGrayColor];
  separator.alpha = 0.5;
  separator.userInteractionEnabled = NO;
  separator.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
  
  
  [self addSubview: separator];
  
}

-(void)updateUI
{
  NSBundle* bundle = [NSBundle bundleForClass:[self class]];
  
  if( self.fullscreen )
  {
    UIImage* image = [UIImage imageNamed:@"HideCandidates"
                                inBundle:bundle
           compatibleWithTraitCollection:nil];
    
    self.moreButton.image = image;
    return;
  }
  // Show More
  NSArray* cells = [self.collectionView visibleCells];
  
  CGFloat width = 0;
  for( UIView *cell in cells )
    width += cell.frame.size.width;
  
  if( self.candidates == nil || self.flowLayout.collectionViewContentSize.width < self.bounds.size.width || self.candidates.count == 0 )
  {
    self.moreButton.hidden = YES;
    self.collectionView.frame = self.bounds;
    
  }else {
    
    self.moreButton.hidden = NO;
    self.collectionView.frame = CGRectMake(0, 0, self.bounds.size.width - self.moreButton.frame.size.width, self.bounds.size.height);
  }
  
  [self setupColors];
  
}

-(void)setTransparent:(BOOL)transparent
{
  _transparent = transparent;
  [self setupColors];
}

-(void)setDarkMode:(BOOL)darkMode
{
  _darkMode = darkMode;
  [self setupColors];
}

-(void)setupColors {
  if( _darkMode && _transparent )
  {
    self.backgroundColor = [UIColor clearColor];
    self.collectionView.backgroundColor = [UIColor colorWithWhite:1 alpha:self.fullscreen?0.0:0.1];
  }
  else if( _darkMode && !_transparent )
  {
    self.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
    
  }
  else if( !_darkMode && !_transparent )
  {
    self.backgroundColor = [UIColor whiteColor];
    self.collectionView.backgroundColor = [UIColor whiteColor];
  }else  {
    self.backgroundColor = [UIColor clearColor];
    self.collectionView.backgroundColor = [UIColor colorWithWhite:1 alpha:self.fullscreen?0.2:0.2];
  }
  [self setNeedsDisplay];
  
  
  NSBundle* bundle = [NSBundle bundleForClass:[self class]];
  
  UIImage* image = [UIImage imageNamed:@"ShowCandidates"
                              inBundle:bundle
         compatibleWithTraitCollection:nil];
  
  if( self.darkMode )
    self.moreButton.tintColor = [UIColor whiteColor];
  else
    self.moreButton.tintColor = [UIColor darkGrayColor];
  
  self.moreButton.image = image;

  if( self.darkMode )
    self.moreButton.backgroundColor = [UIColor clearColor];
  else
    self.moreButton.backgroundColor = [UIColor colorWithWhite:1 alpha: 0.2];
}

-(void)setFullscreen:(BOOL)fullscreen
{
  _fullscreen = fullscreen;
  
  if( self.fullscreen )
  {
    self.moreButton.hidden = YES;
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
  }else
  {
    self.moreButton.hidden = NO;
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  }
}

- (void)layoutSubviews
{
  [super layoutSubviews];
  
  CGFloat viewHeight = CGRectGetHeight(self.bounds);
  
  if( self.fullscreen )
  {
    self.flowLayout.itemSize = CGSizeMake(viewHeight, CANDIDATE_BAR_HEIGHT);
    
  }else {
    
    self.flowLayout.itemSize = CGSizeMake(viewHeight, viewHeight);
  }
  [self.flowLayout invalidateLayout];
}

#pragma mark -

- (BOOL)acceptCurrentCandidate
{
  if (self.selectedIndexPath) {
    [self acceptCandidate:self.selectedIndexPath];
    self.selectedIndexPath = nil;
    return YES;
  }
  
  return NO;
}

- (void)acceptCandidate:(NSIndexPath *)indexPath
{
  InputCandidate *segment = self.candidates[indexPath.item];
  [self.delegate candidateBar:self didAcceptCandidate:segment];
  
  if( self.fullscreen )
  {
    self.flowLayout = nil;
    self.collectionView.dataSource = nil;
    self.collectionView.delegate = nil;
  }
}

- (void)selectPreviousCandidate
{
  NSInteger index = 0;
  NSArray *selectedIndexPaths = self.collectionView.indexPathsForSelectedItems;
  for( NSIndexPath* selectedIndexPath in selectedIndexPaths ) {
    [self collectionView:self.collectionView didUnhighlightItemAtIndexPath:selectedIndexPath];
    index = selectedIndexPath.item - 1;
  }
  
  NSUInteger count = self.candidates.count;
  if (count > 0) {
    if (index <= 0) {
      index = count - 1;
    }
    if (index < count) {
      NSIndexPath *previousIndexPath = [NSIndexPath indexPathForItem:index inSection:0];
      self.selectedIndexPath = previousIndexPath;
      [self.collectionView reloadData];

      [self.collectionView selectItemAtIndexPath:previousIndexPath animated:NO scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
      [self collectionView:self.collectionView didHighlightItemAtIndexPath:previousIndexPath];
    }
  }
}

- (void)selectNextCandidate
{
  NSInteger index = 0;
  NSArray *selectedIndexPaths = self.collectionView.indexPathsForSelectedItems;
  for( NSIndexPath* selectedIndexPath in selectedIndexPaths ) {
    [self collectionView:self.collectionView didUnhighlightItemAtIndexPath:selectedIndexPath];
    index = selectedIndexPath.item + 1;
  }
  
  NSUInteger count = self.candidates.count;
  if (count > 0) {
    if (index == count) {
      index = 0;
    }
    if (index < count) {
      NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:index inSection:0];
      self.selectedIndexPath = nextIndexPath;
      [self.collectionView reloadData];
      [self.collectionView selectItemAtIndexPath:nextIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
      [self collectionView:self.collectionView didHighlightItemAtIndexPath:nextIndexPath];
    }
  }
}

- (InputCandidate *)selectedCandidate
{
  InputCandidate *segment;
  NSIndexPath *selectedIndexPath = self.collectionView.indexPathsForSelectedItems.firstObject;
  if (selectedIndexPath) {
    segment = self.candidates[selectedIndexPath.item];
  }
  return segment;
}

- (void)setCandidates:(NSArray *)candidates
{
  
  
  //#ifdef DEBUG
  //
  //
  //	NSMutableArray* array = [[NSMutableArray alloc] init];
  //	for( NSUInteger hoge = 0 ; hoge < 300 ; hoge++ )
  //	{
  //		InputCandidate* candidate = [[InputCandidate alloc] init];
  //		candidate.input = @"さ";
  //		candidate.candidate = @"差";
  //
  //		[array addObject: candidate];
  //	}
  //
  //	candidates = array;
  //#endif
  
  _candidates = candidates;
  
  NSArray *selectedIndexPaths = self.collectionView.indexPathsForSelectedItems;
  for( NSIndexPath* selectedIndexPath in selectedIndexPaths ) {
    [self collectionView:self.collectionView didUnhighlightItemAtIndexPath:selectedIndexPath];
    [self.collectionView deselectItemAtIndexPath:selectedIndexPath animated:NO];
  }
  [self clearSelection];
  self.collectionView.contentOffset = CGPointZero;
  [self.collectionView reloadData];
  [self updateUI];
}

-(void)clearSelection
{
  self.selectedIndexPath = nil;
  [self.collectionView reloadData];
}

-(void)more:(id)sender
{
  if( [self.delegate respondsToSelector:@selector(candidateBar:showsAll:)] )
  {
    [self.delegate candidateBar:self showsAll:self.candidates];
  }
}


//-(void)drawRect:(CGRect)rect
//{
//
//	CGContextRef ctx = UIGraphicsGetCurrentContext();
//
//
//	if( self.darkMode )
//	{
//		[[UIColor colorWithWhite:1 alpha:0.1] set];
//		UIRectFill(rect);
//
//	}else {
//
//		[[UIColor whiteColor] set];
//		UIRectFill(rect);
//
//	}
//
//
//	CGContextSetStrokeColorWithColor(ctx, RGBA(28, 38, 184, 0.2).CGColor);
//
//	if( self.contentScaleFactor > 1.0 )
//	{
//		CGContextSetLineWidth(ctx, 0.5);
//		CGPoint points[2];
//
//		points[0] = CGPointMake(rect.origin.x, rect.origin.y + 0.25);
//		points[1] = CGPointMake(CGRectGetMaxX(rect), rect.origin.y + 0.25);
//
//
//		CGContextStrokeLineSegments(ctx, points, 2);
//
//	}else
//	{
//		CGContextSetLineWidth(ctx, 1.0);
//
//		CGPoint points[2];
//
//		points[0] = CGPointMake(rect.origin.x, rect.origin.y + 0.5);
//		points[1] = CGPointMake(CGRectGetMaxX(rect), rect.origin.y + 0.5);
//
//
//		CGContextStrokeLineSegments(ctx, points, 2);
//
//	}
//}

#pragma mark -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return MIN( CANDIDATE_MAXIMUM, self.candidates.count);
}

-(NSString*)titleForIndexPath:(NSIndexPath*)indexPath
{
  NSInteger item = indexPath.item;
  InputCandidate *segment = self.candidates[item];
  
  NSString* actualTitle = segment.candidate;
  
  return actualTitle;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  NSInteger item = indexPath.item;
  
  KeyboardCandidateBarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
  InputCandidate *segment = self.candidates[item];
  
  
  NSString* actualTitle = segment.candidate;
  
  
  if( self.fullscreen )
    actualTitle = [segment.candidate truncatedStringWithCTFont:(__bridge CTFontRef)([KeyboardCandidateBarCell font]) width:self.collectionView.frame.size.width - 20 ];
  
  
  cell.text = actualTitle;
  
  cell.showsTopSeparator = item < self.candidates.count;
  cell.showsBottomSeparator = item == self.candidates.count - 1;
  
  [cell setDarkMode:self.darkMode];
  cell.contentView.backgroundColor = [UIColor clearColor];
  
  if( self.selectedIndexPath != nil && self.selectedIndexPath.item == indexPath.item )
  {
    if( _darkMode ) {
      cell.contentView.backgroundColor = [UIColor colorWithRed:0.773 green:0.780 blue:0.820 alpha:1.000];
    }else {
      cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
    }
  }
  return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  CGSize size;
  InputCandidate *segment = self.candidates[indexPath.item];
  
  @autoreleasepool {
    size = [segment.candidate sizeWithAttributes:@{ (NSString*)kCTFontAttributeName: (__bridge id)cellFont_}];
  }
  
  CGFloat itemHeight = self.flowLayout.itemSize.height;
  
  if( self.fullscreen )
  {
    itemHeight = 34;
    return CGSizeMake(size.width + 20.0, itemHeight + 10);
    
  }else {
    return CGSizeMake(size.width + 20.0, itemHeight);
    
  }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  [self acceptCandidate:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
  UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
  
  if( _darkMode ) {
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.773 green:0.780 blue:0.820 alpha:1.000];
  }else {
    cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
  }
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
  UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
  cell.contentView.backgroundColor = nil;
}

- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath{
  
}
@end


