//
//  KeyboardFullscreenCandidateCollectionView.h
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project25.
//  Copyright (c) 2014年 Masatoshi Nishikata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardFullscreenCandidateCollectionView : UIView
{
	NSRange range_;
	NSMutableArray* rangeStack_;
	BOOL enabled_;
}
-(void)enable;

@property (nonatomic, weak) id <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> delegate;

@end


/*
Informal Protocol
 
 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath

 - (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section

*/

