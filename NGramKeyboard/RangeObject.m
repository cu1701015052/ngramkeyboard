//
//  RangeObject.m
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright (c) 2014 Masatoshi Nishikata. All rights reserved.
//

#import "RangeObject.h"

@interface RangeObject ()
@property (nonatomic, readwrite) NSMutableArray* candidates;
@end


@implementation RangeObject

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.enclosingRect = CGRectZero;
	}
	return self;
}

-(NSString *)description
{
	if( self.candidates )
	{
		NSMutableString*  mstr = [[NSMutableString alloc] init];
		
		[mstr appendString:self.text];
		[mstr appendString:@" : "];
		
		[mstr appendString:NSStringFromRange(self.textRange)];
		
		for( NSString* candidate in self.candidates )
		{
			[mstr appendString: @", "];
			[mstr appendString: candidate];
			
		}
		
		return mstr;
		
	}
	
	return [NSString stringWithFormat:@"%@ %@ (stroke %@)", self.text, NSStringFromRange(self.textRange),  NSStringFromRange(self.strokeRange)];
	
}

-(void)addCandidate:(NSString*)string
{
	if( _candidates == nil ) _candidates = [[NSMutableArray alloc] init];
	
	[_candidates addObject:string];
}


@end
