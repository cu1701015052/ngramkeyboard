//
//  KeyboardFullscreenCandidateCollectionView.m
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project25.
//  Copyright (c) 2014年 Masatoshi Nishikata. All rights reserved.
//

#import "KeyboardFullscreenCandidateCollectionView.h"
#import "KeyboardCandidateBarCell.h"
#import "MozcKeyButton.h"
#import <CoreText/CoreText.h>

#define BUTTON_HEIGHT 42

@implementation KeyboardFullscreenCandidateCollectionView

-(void)reloadData
{
	NSInteger hoge = [self.delegate collectionView:(UICollectionView*)self numberOfItemsInSection:0];

	range_.location = 0;
	range_.length = hoge;

	
	[self setNeedsLayout];
}

-(void)enable
{
	enabled_ = YES;
	[self setNeedsLayout];
}

-(void)setDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>)delegate
{
	_delegate = delegate;
	[self reloadData];
}

-(void)layoutSubviews
{
	if( !self.window ) return;
	if( !enabled_ ) return;
	
	const UIEdgeInsets inset = UIEdgeInsetsMake(10, 10, 10, 10);
	const CGFloat verticalSpacing = 5;
	const CGFloat horizontalSpacing = 10;
	
	if( [[(id)self.delegate valueForKey:@"_darkMode"] boolValue] )
	{
		self.tintColor = [UIColor whiteColor];
	}else
	{
		self.tintColor = [UIColor blackColor];
	}
	
	
  @autoreleasepool {
    NSArray* subviews = self.subviews;
    [subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
  }
  
	NSInteger numberOfItems = [self.delegate collectionView:(UICollectionView*)self numberOfItemsInSection:0];
	
	CGPoint point = CGPointMake(inset.left, inset.top);
	
  [MozcTodayEnterButton setFont:(__bridge CTFontRef)[KeyboardCandidateBarCell font] ];
	
	// Loop Start
	const CGFloat nextButtonWidth = 60;
	
	NSInteger index = range_.location;
	range_.length = 0;
	while( index < numberOfItems )
	{
		@autoreleasepool {
			
			if( point.x == inset.left && point.y == inset.top && index != 0 )
			{
				// add Prev button
				MozcTodayEnterButton* button = [[MozcTodayEnterButton alloc] init];
				button.frame = CGRectMake(point.x, point.y, nextButtonWidth, BUTTON_HEIGHT);
				button.selected = YES;
				
				if( [[(id)self.delegate valueForKey:@"_darkMode"] boolValue] )
				{
					button.tintColor = [UIColor whiteColor];
				}else
				{
					button.tintColor = [UIColor darkGrayColor];
				}
				
				button.title = NSLocalizedString(@"Previous", @"");
				button.target = self;
				button.action = @selector(previous:);
				
				[self addSubview: button];
				
				point.x += nextButtonWidth + horizontalSpacing;
				continue;
      }
      
      // Get size
      NSIndexPath* indexPath = [NSIndexPath indexPathForItem:index inSection:0];
      
      CGSize size;
      @autoreleasepool {
        size = [self.delegate collectionView:(UICollectionView*)self
                                      layout:nil
                      sizeForItemAtIndexPath:indexPath];
      }
      BOOL isPenultimateRow = NO;
      BOOL isLastRow = NO;
      
      if( point.y + size.height + verticalSpacing + size.height + verticalSpacing + size.height > self.bounds.size.height - inset.bottom )
      {
        isPenultimateRow = YES;
      }
      
      
      if( point.y + size.height + verticalSpacing + size.height > self.bounds.size.height - inset.bottom )
      {
        isLastRow = YES;
        isPenultimateRow = NO;
      }
      
      if(  !isLastRow && point.x != inset.left && point.x + size.width > self.bounds.size.width - inset.right 	)
      {
        // move to Next row
        
        point.x = inset.left;
        point.y += size.height + verticalSpacing;
      }
      
      if( isLastRow )
      {
        // If last row and last item
        if( point.x + MAX( size.width, nextButtonWidth) > self.bounds.size.width - inset.right - horizontalSpacing - nextButtonWidth )
        {
          // Add next button then end
          
          CGFloat poinX =  self.bounds.size.width - inset.right - nextButtonWidth;
					
					MozcTodayEnterButton* button = [[MozcTodayEnterButton alloc] init];
					button.frame = CGRectMake(poinX, point.y, nextButtonWidth, BUTTON_HEIGHT);
					button.selected = YES;
					button.title = NSLocalizedString(@"Next", @"");
					button.target = self;
					button.action = @selector(next:);

					if( [[(id)self.delegate valueForKey:@"_darkMode"] boolValue] )
					{
						button.tintColor = [UIColor whiteColor];
					}else
					{
						button.tintColor = [UIColor darkGrayColor];
					}
					
					[self addSubview: button];
					
					point = CGPointMake(inset.left, inset.top);
					break;
				}
			}
			// Add item
			
			MozcAlternativeButton* button = [[MozcAlternativeButton alloc] initWithFrame:CGRectMake(point.x, point.y, size.width, size.height) ];
			button.frame = CGRectMake(point.x, point.y, size.width, size.height);
			button.backgroundColor = [UIColor clearColor];
			button.opaque = NO;
			button.tag = index;
			button.target = self;
			button.action = @selector(accept:);
			
			NSString* title = [self.delegate performSelector:@selector(titleForIndexPath:) withObject:indexPath];
			
			button.title = title;

			if( [[(id)self.delegate valueForKey:@"_darkMode"] boolValue] )
			{
				button.titleColor = [UIColor whiteColor];
			}else
			{
				button.titleColor = [UIColor blackColor];
			}
			
			[self addSubview: button];
			
			point.x += size.width + horizontalSpacing;
			
			if( point.x > self.bounds.size.width - inset.right )
			{
				point.x = inset.left;
				point.y += size.height + verticalSpacing;
			}
			
			index ++ ;
			range_.length ++;
			
			// Loop end
		}
	}
}

-(void)accept:(UIView*)button
{
	NSIndexPath* indexPath = [NSIndexPath indexPathForItem:button.tag inSection:0];

	[self.delegate performSelector:@selector(acceptCandidate:) withObject:indexPath];
}

-(void)next:(id)sender
{
	if( !rangeStack_ ) rangeStack_ = [[NSMutableArray alloc] init];
	[rangeStack_ addObject:@(range_.location)];
	
	range_.location = NSMaxRange(range_);
	[self setNeedsLayout];
}

-(void)previous:(id)sender
{

	if( !rangeStack_ || rangeStack_.count == 0 ) return;
	
	NSNumber* obj = [rangeStack_ lastObject];
	
	[rangeStack_ removeLastObject];
	
	range_.location = obj.integerValue;
	[self setNeedsLayout];
}

#pragma mark - For compatibility

-(NSArray*)indexPathsForSelectedItems
{
	return nil;
}

-(void)setContentOffset:(CGPoint)offset
{
}

-(CGPoint)contentOffset
{
	return CGPointZero;
}

-(void)setDataSource:(id)sender
{
	
}

@end



