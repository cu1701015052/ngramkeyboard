//
//  KeyButton.h
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project05.
//  Copyright (c) 2014年 Masatoshi Nishikata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface MozcAlternativeButton: UIView
{
  NSTimeInterval hiddenTime;
  NSTimer * hiddenTimer;
}
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) UIColor *titleColor;

@property (nonatomic, readwrite) BOOL highlighted;
@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) UIImage* highlightImage;
@property (nonatomic, weak) id <NSObject> target;
@property (nonatomic, readwrite) SEL action;
@property (nonatomic, readwrite) BOOL keyboardClick;

+(void)setFont:(CTFontRef)newFont;
-(CTFontRef)font;
-(void)setTarget:(id<NSObject>)target action:(SEL)selector;

@end

@interface MozcKeyButton : MozcAlternativeButton
{
	NSTimer* timer_;
}

@end


@interface MozcTodayEnterButton : MozcAlternativeButton
@property (nonatomic, readwrite) BOOL selected;

@end
