//
//  Ngram.swift
//  NGarmKeyboard
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2018 Masatoshi Nishikata. All rights reserved.
//

import NaturalLanguage
import Realm
import RealmSwift

struct NgramEntry: Equatable, Hashable, Comparable {
  static func < (lhs: NgramEntry, rhs: NgramEntry) -> Bool {
    return lhs.strings.joined(separator: "") < rhs.strings.joined(separator: "")
  }
  
  var strings: [String] = []
  var count: Int = 0
  
  static func ==(lhs: NgramEntry, rhs: NgramEntry) -> Bool {
    return lhs.strings == rhs.strings
  }
  
  var hashValue: Int {
    return strings.first?.hashValue ?? 0
  }
  
  var description: String {
    let string = "（" + strings.joined(separator: ", ") +  "）" + String(count)
    return string
  }
  
}

class NgramEntryObj: Object {
  @objc dynamic var string0: String = ""
  @objc dynamic var string1: String = ""
  @objc dynamic var string2: String = ""
  @objc dynamic var count: Int = 0
  
  override static func indexedProperties() -> [String] {
    return ["string0", "string1", "string2", "count"]
  }
  
  
  override static func ignoredProperties() -> [String] {
    return ["strings", "description"]
  }
  
  var strings: [String] {
    get {
    return [string0, string1, string2]
    }
    set {
      string0 = newValue[0]
      string1 = newValue[1]
      string2 = newValue[2]
    }
  }

  override var description: String {
    let string = "（" + strings.joined(separator: ", ") +  "）" + String(count)
    return string
  }
  
}

class Ngram {
  static let delimiter = ["「", "」", "（", "）", "。", "、", "\n", "　"]
  
  
  func letterNgram(_ n: Int, in text: NSString) -> [NgramEntry] {
    var entries: [NgramEntry] = []
    
    for hoge in 0..<text.length - n + 1  {
      let startIndice = (hoge..<hoge+n)
      let letters = startIndice.map { text.substring(with: NSRange(location: $0, length: 1)) }
      
      entries.append( NgramEntry(strings: letters, count: 0) )
    }
    
    entries.sort()
    
    var countedEntries: [NgramEntry] = []
    loop: for entry in entries {
      if 0 !=  entry.strings.filter({ Ngram.delimiter.contains($0) }).count { continue }
      
      if countedEntries.last == entry {
        countedEntries[countedEntries.count-1].count += 1

      }else {
        countedEntries.append(entry)
      }
    }
    
    countedEntries = countedEntries.filter {  $0.count > 0  }

    countedEntries.sort {  $0.count > $1.count }
    
    return countedEntries
  }
  
  
  func wordNgram__(_ n: Int, in text: NSString) -> [NgramEntry] {
    
    var words: [String] = []
    
    //テキストが長くなると動かなくかる
    /*
     let tokenizer = NLTokenizer(unit: .word)
     tokenizer.string = text as String
     tokenizer.setLanguage(NLLanguage.japanese)
     tokenizer.enumerateTokens(in: (text as String).startIndex..<(text as String).endIndex) { tokenRange, attributes in
     words.append(String((text as String)[tokenRange]))
     return true
     }
     */
    
    let tagger = NLTagger(tagSchemes: [.lexicalClass])
    tagger.string = text as String
    let options: NLTagger.Options = [ .omitWhitespace]
    tagger.enumerateTags(in: (text as String).startIndex..<(text as String).endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
      if let _ = tag {
        words.append(String((text as String)[tokenRange]))
      }
      return true
    }
    
    
    var entries: [NgramEntry] = []
    
    for hoge in 0..<words.count - n + 1  {
      let startIndice = (hoge..<hoge+n)
      let letters = startIndice.map { words[$0] }
      
      entries.append( NgramEntry(strings: letters, count: 0) )
    }
    
    entries.sort()
    
    var countedEntries: [NgramEntry] = []
    loop: for entry in entries {
      if 0 !=  entry.strings.filter({ Ngram.delimiter.contains($0) }).count { continue }
      
      if countedEntries.last == entry {
        countedEntries[countedEntries.count-1].count += 1
        
      }else {
        countedEntries.append(entry)
      }
    }
    
    countedEntries = countedEntries.filter {  $0.count > 0  }
    
    countedEntries.sort {  $0.count > $1.count }
    
    return countedEntries
  }

  
  func ngram(named name: String) -> Realm? {
    let cachesDirectoryPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
    let cachesDirectoryURL = NSURL(fileURLWithPath: cachesDirectoryPath)
    let url = cachesDirectoryURL.appendingPathComponent("\(name).realm")!

    if FileManager.default.fileExists(atPath: url.path) == false { return nil }
    let config = Realm.Configuration(fileURL: url, readOnly: true, objectTypes:[NgramEntryObj.self])
    let realm = try? Realm(configuration: config)

    return realm
  }
  
  func generateWordNgram(with text: String, name: String, progress:@escaping (_ progress: Double)->Void ,completion: @escaping (_ realm: Realm?)->Void)  {
    let n = 3
    var words: [String] = []

    let config = Realm.Configuration(inMemoryIdentifier: "temp_ngram",  objectTypes:[NgramEntryObj.self])
    
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
    queue.async {
      
      let realm = try! Realm(configuration: config)
      
      let tagger = NLTagger(tagSchemes: [.lexicalClass])
      tagger.string = text
      let options: NLTagger.Options = [.omitWhitespace]
      tagger.enumerateTags(in: text.startIndex..<text.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
        if let _ = tag {
          let subtext = String((text as String)[tokenRange])
          words.append(subtext)
        }
        return true
      }
      
      var allEntries: [String: NgramEntryObj] = [:]
      
      let count = words.count - n + 1
      var hoge = 0
      while hoge < count  {
        if hoge%10 == 0 {
          DispatchQueue.main.async {
            progress(Double(hoge)/Double(count))
          }
        }
        let startIndice = (hoge..<hoge+n)
        let letters = startIndice.map { words[$0] }
        
        if 0 != letters[2].filter({ Ngram.delimiter.contains(String($0)) }).count { hoge += 3; continue }
        if 0 != letters[1].filter({ Ngram.delimiter.contains(String($0)) }).count { hoge += 2; continue }
        if 0 != letters[0].filter({ Ngram.delimiter.contains(String($0)) }).count { hoge += 1; continue }

        autoreleasepool {
          let key = letters.joined(separator: "")
          if let entry = allEntries[key] {
            entry.count += 1
          }else {
            let entry = NgramEntryObj()
            entry.strings = letters
            allEntries[key] = entry
          }
        }
        
        hoge += 1
      }
      
      try? realm.write {
        realm.add(allEntries.values)
      }
      
      let cachesDirectoryPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
      let cachesDirectoryURL = NSURL(fileURLWithPath: cachesDirectoryPath)
      let destUrl = cachesDirectoryURL.appendingPathComponent("\(name).realm")!

      try? realm.writeCopy(toFile: destUrl)
      if let url = realm.configuration.fileURL {
      try? FileManager.default.removeItem(at: url)
      }
      DispatchQueue.main.async {
        let config = Realm.Configuration(fileURL: destUrl,  readOnly: true, objectTypes:[NgramEntryObj.self])
        let realm = try! Realm(configuration: config)

        completion(realm)
      }
    }
  }
  
  func getLastTwoWords(in text: String) -> [String] {
    
    var words: [String] = []
    let tagger = NLTagger(tagSchemes: [.lexicalClass])
    tagger.string = text
    let options: NLTagger.Options = [ .omitWhitespace]
    tagger.enumerateTags(in: (text as String).startIndex..<(text as String).endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
      if let _ = tag {
        words.append(String((text as String)[tokenRange]))
      }
      return true
    }
    
    
    var val: [String] = []
    if let last = words.last {
      val.append(last)
    }
    words = Array(words.dropLast())
    
    if let last = words.last {
      val.append(last)
    }
    return val
  }
}
