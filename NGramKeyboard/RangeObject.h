//
//  RangeObject.h
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright (c) 2014 Masatoshi Nishikata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface RangeObject : NSObject

-(void)addCandidate:(NSString*)string;

@property (nonatomic, readwrite) NSRange strokeRange;
@property (nonatomic, readwrite) NSRange textRange;
@property (nonatomic, copy) NSString* text;
@property (nonatomic, readonly) NSMutableArray* candidates;

// Calculated
@property (nonatomic, readwrite) CGRect enclosingRect;


@end
