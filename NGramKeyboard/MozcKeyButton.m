//
//  KeyButton.m
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project05.
//  Copyright (c) 2014年 Masatoshi Nishikata. All rights reserved.
//

#import "MozcKeyButton.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation MozcAlternativeButton

static CTFontRef buttonFont_ = nil;
extern CGFloat gContentScaleFactor;
-(instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = [UIColor clearColor];
		self.opaque = NO;
		self.contentMode = UIViewContentModeRedraw;
    self.contentScaleFactor = gContentScaleFactor;

	}
	return self;
}

-(CGSize)sizeThatFits:(CGSize)size
{
  if( _image != nil ) return _image.size;
  return size;
}

-(void)setContentScaleFactor:(CGFloat)contentScaleFactor
{
  [super setContentScaleFactor:gContentScaleFactor];
}

-(void)setTarget:(id<NSObject>)target action:(SEL)selector {
  self.target = target;
  self.action = selector;
}

+(CTFontRef)font
{
	if( buttonFont_ == nil ) {
		buttonFont_ = CTFontCreateWithName(CFSTR("HelveticaNeue"), 18, nil);
	}
	return buttonFont_;
}

+(void)setFont:(CTFontRef)newFont
{
  if( buttonFont_ != nil ) CFRelease(buttonFont_);
  buttonFont_ = newFont;
  CFRetain(newFont);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	//[super touchesBegan:touches withEvent:event];
	
	self.highlighted = YES;
  
  hiddenTime = [NSDate timeIntervalSinceReferenceDate];
  [hiddenTimer invalidate];
  hiddenTimer = nil;
  
  if( self.keyboardClick )
    AudioServicesPlaySystemSound(0x450);
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	if( self.highlighted ) {
		if( [self.target respondsToSelector:self.action] )
		{
			[self.target performSelector:self.action withObject:self];
		}
	}

  if( hiddenTime != 0 && [NSDate timeIntervalSinceReferenceDate] - hiddenTime < 0.1 ) {
    
    NSTimeInterval time = ( 0.1 - ([NSDate timeIntervalSinceReferenceDate] - hiddenTime ) );
    
    [hiddenTimer invalidate];
    
    hiddenTimer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(fireHiding) userInfo:nil repeats:NO];
    
  }else {
    hiddenTime = 0;
    self.highlighted = NO;
  }
}

-(void)fireHiding
{
  [hiddenTimer invalidate];
  hiddenTimer = nil;
  hiddenTime = 0;
  self.highlighted = NO;
}

/*
-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
  //self.highlighted = NO;
}
*/

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	self.highlighted = NO;
}

-(void)setImage:(UIImage*)image
{
	_image = image;
  super.contentScaleFactor = 2.0;

	[self setNeedsDisplay];
}

-(void)setHighlighted:(BOOL)highlighted
{
	_highlighted = highlighted;
	[self setNeedsDisplay];
}

-(void)setEnabled:(BOOL)enabled
{
	[self setUserInteractionEnabled:enabled];
	[self setNeedsDisplay];
	
	//	self.hidden = !enabled;
}

-(void)setTintColor:(UIColor *)tintColor
{
  [super setTintColor:tintColor];
  [self setNeedsDisplay];
}

-(NSDictionary*)attributes
{
	NSDictionary* attributes = @{ (__bridge id)kCTFontAttributeName : (__bridge id)[MozcAlternativeButton font],
											NSForegroundColorAttributeName : self.tintColor};
	return attributes;
}

-(void)drawRect:(CGRect)rect
{
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	[self.tintColor set];

	if( _image != nil ) {
		
		CGSize size = [_image size];
    CGPoint point = CGPointMake( (rect.size.width - size.width)/2, (rect.size.height - size.height)/2);

    if( _highlighted || !self.userInteractionEnabled )
    {
      if( _highlightImage != nil )
      {
        [_highlightImage drawAtPoint:point blendMode:kCGBlendModeNormal alpha:0.5];
        
      }else {
        [_image drawAtPoint:point blendMode:kCGBlendModeNormal alpha:0.5];
      }
			
		}else {
			CGContextSaveGState(ctx);
			
			CGAffineTransform t = CGAffineTransformMakeScale(1, -1);
			t = CGAffineTransformTranslate(t, 0, -rect.size.height);
			CGContextConcatCTM(ctx, t);
			
			CGRect imageRect = CGRectMake(point.x, point.y, _image.size.width, _image.size.height);
			CGContextClipToMask(ctx, imageRect, _image.CGImage);
			CGContextFillRect(ctx, imageRect);
			
			//[_image drawAtPoint:point blendMode:kCGBlendModeNormal alpha:1];
			
			CGContextRestoreGState(ctx);		}
		return;
	}
	
	
	NSDictionary* attributes = self.attributes;
	
	CGSize size = [self.title sizeWithAttributes:attributes];
	
	CGPoint origin = CGPointMake((self.bounds.size.width - size.width)/2, (self.bounds.size.height - size.height)/2);
	
	
	
	if( _highlighted )
	{
		[[UIColor colorWithWhite:1 alpha:0.8] set];
		UIRectFill(rect);
		[self.title drawAtPoint:origin withAttributes:attributes];
		
	}else
	{
		[self.title drawAtPoint:origin withAttributes:attributes];
	}
	
	return;
	
	
}
@end

@implementation MozcKeyButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	
	if( [self.target respondsToSelector:self.action] )
	{
		[self.target performSelector:self.action withObject:self];
	}
		
	[timer_ invalidate];
	timer_ = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(fire) userInfo:nil repeats:NO];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
  if( hiddenTime != 0 && [NSDate timeIntervalSinceReferenceDate] - hiddenTime < 0.1 ) {
    
    NSTimeInterval time = ( 0.1 - ([NSDate timeIntervalSinceReferenceDate] - hiddenTime ) );
    
    [hiddenTimer invalidate];
    
    hiddenTimer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(fireHiding) userInfo:nil repeats:NO];
    
  }else {
    hiddenTime = 0;
    self.highlighted = NO;
  }
	
	[timer_ invalidate];
	timer_ = nil;
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	[super touchesCancelled:touches withEvent:event];
	
	[timer_ invalidate];
	timer_ = nil;
	
}

-(void)fire{
	
	if( [self.target respondsToSelector:self.action] )
	{
		[self.target performSelector:self.action withObject:self];
	}
	[timer_ invalidate];
	timer_ = [NSTimer scheduledTimerWithTimeInterval:0.15 target:self selector:@selector(fire) userInfo:nil repeats:NO];

  if( self.keyboardClick )
    AudioServicesPlaySystemSound(0x450);

}
@end


@implementation MozcTodayEnterButton


-(CGFloat)width
{
	return ceilf( [self.title sizeWithAttributes: self.attributes].width + 20 );
}


-(void)setSelected:(BOOL)selected
{
	if( self.selected == selected ) return;
	_selected = selected;
	[self setNeedsDisplay];
}

-(void)setTintColor:(UIColor *)tintColor
{
	[super setTintColor:tintColor];
	[self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect
{
	
	NSDictionary* attributes = self.attributes;
	
	CGSize size = [self.title sizeWithAttributes:attributes];
	
	CGPoint origin = CGPointMake((self.bounds.size.width - size.width)/2, (self.bounds.size.height - size.height)/2);
	
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	
	
	
	[self.tintColor set];
	
	if( !self.selected )
	{
		if( self.highlighted )
		{
			[[UIColor colorWithWhite:0 alpha:0.3] set];
			UIRectFill(rect);
			[self.title drawAtPoint:origin withAttributes:attributes];
			
		}else
		{
			[self.title drawAtPoint:origin withAttributes:attributes];
		}
		
		return;
	}
	
	if( self.highlighted || !self.userInteractionEnabled )
	{
		UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.selectionFrame cornerRadius:6];
		[path stroke];
		
		[self.title drawAtPoint:origin withAttributes:attributes];
		
	}else
	{
		UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.selectionFrame cornerRadius:6];
		[path fill];
		
		
		CGContextSaveGState(ctx);
		
		CGContextSetBlendMode(ctx, kCGBlendModeClear);
		[self.title drawAtPoint:origin withAttributes:attributes];
		CGContextRestoreGState(ctx);
	}
	
}

-(CGRect)selectionFrame
{
	CGRect fillRect = CGRectInset(self.bounds, 5, 5);
	return fillRect;
}

@end

