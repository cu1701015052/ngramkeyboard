//
//  NSString+EventTableCellDrawing.h
//  Speak
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project07.
//  Copyright (c) 2014年 Masatoshi Nishikata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface NSString (EventTableCellDrawing)
-(NSString*)truncatedStringWithFont:(UIFont*)font width:(CGFloat)width;
-(NSString*)truncatedStringWithCTFont:(CTFontRef)font width:(CGFloat)width;

@end

