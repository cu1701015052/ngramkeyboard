//
//  ViewController.swift
//  ngramKeyboard
//
//  Created by Masatoshi Nishikata for Cyber University Graduation Project.
//  Copyright © 2018 Masatoshi Nishikata. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class ViewController: UIViewController, UITextViewDelegate {
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var segment: UISegmentedControl!
  
  weak var candidateBar: KeyboardCandidateBar?
  weak var loadingIndicator: CircularProgressIndicator?

  var ngram: Realm?
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    // LOAD INITIAL NGRAM
    let name = segment.selectedSegmentIndex == 0 ? "minji" : "tokkyo"
    ngram = Ngram().ngram(named: name)
    if ngram == nil {
      loadNGram()
    }
    
   updateUI()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    textView.becomeFirstResponder()
  }
  
  func loadNGram() {
    let name = segment.selectedSegmentIndex == 0 ? "minji" : "tokkyo"
    let url = Bundle.main.url(forResource: name, withExtension: "txt")!
    let text = try! String(contentsOf: url)
    Ngram().generateWordNgram(with: text, name: name, progress: { (progress) in
      self.loadingIndicator?.progress = progress
    }) { (realm) in
      if realm != nil {
        self.ngram = realm
        self.updateUI()
      }
    }
    
  }
  
  func updateUI() {
    if ngram != nil {
      
      let barHeight = 50
      
      let barFrame = CGRect(x: 0, y: 0, width: Int(textView.frame.size.width), height: barHeight)
      let candidateBar = KeyboardCandidateBar(frame: barFrame)
      candidateBar.autoresizingMask = [.flexibleBottomMargin, .flexibleWidth]
      candidateBar.delegate = self
      candidateBar.fullscreen = false
      candidateBar.darkMode = false
      candidateBar.transparent = false
      self.candidateBar = candidateBar
      textView.inputAccessoryView = candidateBar
      textView.reloadInputViews()
    }else {
      let barHeight = 50

      let barFrame = CGRect(x: 0, y: 0, width: Int(textView.frame.size.width), height: barHeight)
      let loadingView = UIView(frame: barFrame)
      loadingView.autoresizingMask = [.flexibleBottomMargin, .flexibleWidth]

      let view = CircularProgressIndicator(frame: CGRect(x: 15, y: 7, width: 36, height: 36))
      view.lineWidth = 2
      view.indeterminate = true
      view.autoresizingMask = [.flexibleRightMargin]
      loadingView.addSubview(view)
      loadingIndicator = view
      
      let label = UILabel(frame: CGRect(x: 55, y: 7, width: 300, height: 36))
      label.text = "Generating N-gram..."
      label.textColor = UIColor.gray
      label.autoresizingMask = [.flexibleRightMargin]

      loadingView.addSubview(label)

      textView.inputAccessoryView = loadingView
      textView.reloadInputViews()

    }
    
  }
  
  @IBAction func segmentSelected(_ sender: Any) {
    let name = segment.selectedSegmentIndex == 0 ? "minji" : "tokkyo"
    ngram = Ngram().ngram(named: name)
    if ngram == nil {
      loadNGram()
    }
    updateUI()
  }
  
  func textViewDidChange(_ textView: UITextView) {
    if let range = textView.markedTextRange {
      let text = textView.text(in: range)
      let manager = InputManager.shared()
      manager?.requestCandidates(forInput: text)
      manager?.delegate = self
    }else {
      textViewDidChangeSelection(textView)
    }
  }
  

  func textViewDidChangeSelection(_ textView: UITextView) {
    guard textView.markedTextRange == nil else { return }
    guard textView.selectedRange.location != 0 else { candidateBar?.candidates = []; return }
    guard let ngram = ngram else { return }
    let maxCandidates = 8

    //GET LAST WORD
    let n = min(textView.selectedRange.location, 10)
    
    let substring = (textView.text as NSString).substring(with: NSMakeRange(textView.selectedRange.location - n, n))
    
  
    let lastWords = Ngram().getLastTwoWords(in: substring)
    var ngramCandidates: [InputCandidate] = []
    
    if lastWords.count == 2 {
      
      
      let predicate = NSPredicate(format: "string0 == %@ && string1 BEGINSWITH %@ ", lastWords[1], lastWords[0])

      let relevantNgrams = ngram.objects(NgramEntryObj.self).filter(predicate).sorted(byKeyPath: "count", ascending: false)
      

      for n in 0..<min(maxCandidates, relevantNgrams.count) {
        var string = relevantNgrams[n].strings.joined(separator: "")
        let original = lastWords[1] + lastWords[0]
        
        string = (string as NSString).substring(from: original.count)
        
        ngramCandidates.append(InputCandidate(input: "", candidate: string))
      }
    }
    
    if lastWords.count > 0 {
      
      let predicate = NSPredicate(format: "string0 BEGINSWITH %@ ", lastWords[0])
      let relevantNgrams = ngram.objects(NgramEntryObj.self).filter(predicate).sorted(byKeyPath: "count", ascending: false)
      
      for n in 0..<min(maxCandidates, relevantNgrams.count) {
        var string = relevantNgrams[n].strings.joined(separator: "")
        let original = lastWords[0]
        
        string = (string as NSString).substring(from: original.count)
        
        ngramCandidates.append(InputCandidate(input: "", candidate: string))
      }
    }
    

    
    candidateBar?.candidates = ngramCandidates
    
  }
  
}

extension ViewController: InputManagerDelegate {
  func inputManager(_ inputManager: InputManager!, didCompleteWith candidates: [InputCandidate]!) {
    guard textView.markedTextRange != nil else { return }
    guard let ngram = ngram else { return }
    let maxCandidates = 10

    var allRelevantNgrams: [NgramEntryObj] = []
    
    for n in 0..<min(candidates.count, 5) {
      let candidate = candidates[n]
      
      let predicate = NSPredicate(format: "string0 BEGINSWITH %@ ", candidate.candidate)
      let relevantNgrams = ngram.objects(NgramEntryObj.self).filter(predicate).sorted(byKeyPath: "count", ascending: false)
      
      allRelevantNgrams += relevantNgrams
    }
    
    
    var ngramCandidates: [InputCandidate] = []
    for n in 0..<min(allRelevantNgrams.count, maxCandidates) {
      
      var string = allRelevantNgrams[n].strings[0] + allRelevantNgrams[n].strings[1]
      let candidate = InputCandidate(input: "", candidate: string)
      if ngramCandidates.contains(candidate) == false {
      ngramCandidates.append(candidate)
      }
      
      string = allRelevantNgrams[n].strings.joined(separator: "")
      ngramCandidates.append(InputCandidate(input: "", candidate: string))
    }
    
    candidateBar?.candidates = ngramCandidates
  }
  
  func inputManager(_ inputManager: InputManager!, didFailWithError error: Error!) {
    
  }

}

extension ViewController: KeyboardCandidateBarDelegate {
  func candidateBar(_ candidateBar: KeyboardCandidateBar!, didAccept segment: InputCandidate!) {
    print(segment)
    if textView.markedTextRange != nil {
      var selectedRange = textView.selectedRange
      textView.setMarkedText(segment.candidate, selectedRange: NSMakeRange(0, 1))
      textView.unmarkText()
      selectedRange.location += segment.candidate.count
      textView.selectedRange = NSMakeRange(selectedRange.location, 0)
      
      
    }else {
      var selectedRange = textView.selectedRange
      selectedRange.location -= segment.input.count
      selectedRange.length += segment.input.count
      textView.selectedRange = selectedRange
      textView.insertText(segment.candidate)
      
      selectedRange.location += segment.candidate.count
      selectedRange.length = 0
      textView.selectedRange = selectedRange

    }
  }

}
